﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LayersGamePrototype
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Phases : ContentPage
	{
        ObservableCollection<int> phaseList = new ObservableCollection<int>();
        public Phases ()
		{
			InitializeComponent ();
            MessagingCenter.Subscribe<Application>(this, "t", (sender) =>
            {
                //Detects if the user is starting a game
                PopulateList();
            });
        }
        void PopulateList()
        {
            phaseList.Clear();
            for(int i = 1; i <= GlobalVars.maxPhase; i++)
            {
                phaseList.Add(i);
            }
            listView.ItemsSource = phaseList;
        }
        void Handle_Click(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ListView selection = (ListView)sender;
            String l = selection.SelectedItem.ToString();
            //Change viewed phase
            GlobalVars.currentPhase = (int)Char.GetNumericValue(l[0]);
            MessagingCenter.Send<Application>(Application.Current, "c");
        }
	}
}