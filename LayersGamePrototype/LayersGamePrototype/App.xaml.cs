﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Xamarin.Forms;
using System.Runtime.CompilerServices;
using System.IO;
using System.Reflection;

namespace LayersGamePrototype
{
    public static class GlobalVars
    {
        public static int currentRow;
        public static int currentCol;
        public static int currentPhase;
        public static int currentFloor;
        public static int maxPhase;
        public static int maxLevel;
        public static int goalRow;
        public static int goalCol;
        public static int goalFloor;
        public static int viewFloor;
        public static int lastPuz;
        //public static int viewPhase;
        public static bool[,,,] puzzlebox;
    }
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
            LoadBoard(0);
            GlobalVars.lastPuz = LastPuzz();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(App)}:  ctor");
            MainPage = new NavigationPage(new PhasePage
            {
                Master = new Phases(),
                Detail = new NavigatorTabs()
            });
        }
        public int LastPuzz()
        {
            //This gets the full path for the "files" directory of your app, where you have permission to read/write.
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            //This creates the full file path to your "testfile.txt" file.
            var filePath = System.IO.Path.Combine(documentsPath, "last.txt");
            int r = 1;
            StreamReader reader;
            try { reader = new System.IO.StreamReader(filePath); }
            catch (System.IO.FileNotFoundException se)
            {
                StreamWriter writer = new StreamWriter(filePath, false);
                using (writer)
                {
                    writer.WriteLine(1);
                }
                writer.Close();
                reader = new StreamReader(filePath);
            }
            using (reader)
            {
                string line = reader.ReadLine();
                r = Convert.ToInt32(line);
                reader.Close();
            }
            if(r == 0) { r = 1; }
            return r;
        }
        public void LastPuzz(int i)
        {
            //This gets the full path for the "files" directory of your app, where you have permission to read/write.
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            //This creates the full file path to your "testfile.txt" file.
            var filePath = System.IO.Path.Combine(documentsPath, "last.txt");
            //Now create/open the file.
            File.Delete(filePath);
            StreamWriter writer = new StreamWriter(filePath, false);
            using (writer)
            {
                writer.WriteLine(i);
            }
            writer.Close();
        }
        protected override void OnStart ()
		{
            // Handle when your app starts
        }
        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

        public void LoadBoard(int d)
        {
            //This will open the puzzle data file and populate the puzzle box array.
            //Fills the fields from floor 1 to Floor 4, left to right, top to bottom (in that order)
            string[] data = LoadData(d);
            int i = 0;
            string specs = data[i++];
            //The top line of the file will specify the number of floors followed by the number of phases
            GlobalVars.maxLevel = (int)Char.GetNumericValue(specs[0]);
            GlobalVars.maxPhase = (int)Char.GetNumericValue(specs[1]);
            //Then the next 3 values are the starting row/col and floor
            GlobalVars.currentRow = (int)Char.GetNumericValue(specs[2]);
            GlobalVars.currentCol = (int)Char.GetNumericValue(specs[3]);
            GlobalVars.currentFloor = (int)Char.GetNumericValue(specs[4]);
            //These initial parameters will always be the same regardless of starting point
            GlobalVars.currentPhase = 1;
            GlobalVars.viewFloor = 1;
            //GlobalVars.viewPhase = 0;
            //Then the next 3 values are the goal row/col and floor
            GlobalVars.goalRow = (int)Char.GetNumericValue(specs[5]);
            GlobalVars.goalCol = (int)Char.GetNumericValue(specs[6]);
            GlobalVars.goalFloor = (int)Char.GetNumericValue(specs[7]);

            GlobalVars.puzzlebox = new bool[5, 5, GlobalVars.maxLevel, GlobalVars.maxPhase];
            for (int row = 0; row < 5; row++)
            {
                for(int col = 0; col < 5; col++)
                {
                    //Pull the current row/column string from the data array
                    string numbers = data[i++];
                    int floor = 1;
                    foreach(char c in numbers){
                        if(c == '.')
                        {
                            //We use a period to separate the floor data
                            floor++;
                            if (floor > GlobalVars.maxLevel)
                                break;
                        }
                        else
                        {
                            //Converts the text characters into raw numbers
                            int phase = (int)Char.GetNumericValue(c);
                            if(phase <= GlobalVars.maxPhase && floor <= GlobalVars.maxLevel)
                                GlobalVars.puzzlebox[row,col,floor-1,phase-1] = true;
                        }
                    }
                }
            }
        }

        protected string[] LoadData(int id)
        {
            #if __IOS__
            var resourcePrefix = "LayersGamePrototype.iOS.";
#endif
#if __ANDROID__
            var resourcePrefix = "LayersGamePrototype.Droid.";
#endif
            // note that the prefix includes the trailing period '.' that is required
            //ensure all text files are of type "Embedded Resource"
            var assembly = typeof(App).GetTypeInfo().Assembly;
            Stream stream1 = assembly.GetManifestResourceStream(resourcePrefix + "Puzzles." + String.Format("p{0}.txt", id));
            string[] text = new string[26];
            int i = 0;
            using (var reader = new System.IO.StreamReader(stream1))
            {
                while (i < 26) {
                    string line = reader.ReadLine();
                    if (line == null)
                    {
                        reader.Dispose();
                    }
                    else
                    {
                        text[i] = line;
                        i++;
                    }
                }
            }
            stream1.Close();
            return text;
        }

    }
}
