﻿namespace LayersGamePrototype
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Fox
    {
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }
    }

    public partial class Fox
    {
        public static Fox FromJson(string json) => JsonConvert.DeserializeObject<Fox>(json, LayersGamePrototype.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Fox self) => JsonConvert.SerializeObject(self, LayersGamePrototype.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
