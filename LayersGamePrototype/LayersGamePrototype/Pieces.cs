﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace LayersGamePrototype
{
	public class Pieces
	{
		public static void PlacePieces(int floor, Grid floorGrid)
        {
            floorGrid.Children.Clear();
            for (int r = 0; r < 5; r++)
            {
                for (int c = 0; c < 5; c++)
                {
                    if (GlobalVars.puzzlebox[r, c, floor-1, GlobalVars.currentPhase - 1])
                    {
                        //If the bool is true, create an image at that space
                        var L = new Image();
                        if (GlobalVars.goalFloor == floor && GlobalVars.goalCol == c && GlobalVars.goalRow == r)
                        {
                            L.Style = Application.Current.Resources["Goal"] as Style;
                        }
                        else if (GlobalVars.currentCol == c && GlobalVars.currentRow == r && GlobalVars.currentFloor == floor)
                        {
                            L.Style = Application.Current.Resources["Player"] as Style;
                        }
                        else
                        {
                            L.Style = Application.Current.Resources["OpenSpace"] as Style;
                        }
                        var LTap = new TapGestureRecognizer();
                        LTap.Tapped += (s, e) =>
                        {
                            Pieces.GoToPosition(s, e);
                        };
                        L.GestureRecognizers.Add(LTap);
                        floorGrid.Children.Add(L, c, r);
                    }
                }
            }
        }
        public static void SwapPieces(Grid floorGrid)
        {
            //Try to swap just two pieces instead of the whole board?
            foreach(Image j in floorGrid.Children)
            {
                //string s = j.Source.ToString();
                if(j.Source.ToString() == "File: player.png")
                {
                    j.Source = "blank.png";
                    break;
                }
            }
            ((Image)floorGrid.Children.ElementAt(GetIndex(GlobalVars.currentRow, GlobalVars.currentCol, GlobalVars.viewFloor, GlobalVars.currentPhase))).Source = "player.png";
        }
        public static int GetIndex(int row, int col, int floor, int phase)
        {
            //Gets the child index of the piece to be changed
            int res = 0;
            for(int i = 0; i <= row; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    if(i == row && j == col)
                    {
                        return res;
                    }
                    if (GlobalVars.puzzlebox[i, j, floor-1, phase-1])
                    {
                        res++;
                    }
                }
            }
            return res;
        }
        public static void GoToPosition(object sender, System.EventArgs e)
        {
            int x = (int)((Image)sender).X;
            int y = (int)((Image)sender).Y;
            int col = (int)(((Image)sender).X / ((Image)sender).Width);
            int row = (int)(((Image)sender).Y / ((Image)sender).Height);
            if (x > 0) { col++; }
            if(y > 0) { row++; }
            if (GlobalVars.currentFloor == GlobalVars.viewFloor)
            {
                if ((GlobalVars.currentCol == col && (GlobalVars.currentRow == row + 1 || GlobalVars.currentRow == row - 1)))
                {
                    GlobalVars.currentCol = col;
                    GlobalVars.currentRow = row;
                }
                else if ((GlobalVars.currentRow == row && (GlobalVars.currentCol == col + 1 || GlobalVars.currentCol == col - 1)))
                {
                    GlobalVars.currentCol = col;
                    GlobalVars.currentRow = row;
                }
            }
            else if (GlobalVars.currentFloor == GlobalVars.viewFloor + 1 || GlobalVars.currentFloor == GlobalVars.viewFloor - 1)
            {
                if (GlobalVars.currentCol == col && GlobalVars.currentRow == row)
                {
                    GlobalVars.currentFloor = GlobalVars.viewFloor;
                }
            }
            if (DetectWin())
            {
                try { MessagingCenter.Send<Application>(App.Current, "v"); }
                catch(System.Reflection.TargetInvocationException er)
                {
                }
            }
            else
            {
                MessagingCenter.Send<Application>(App.Current, "n");
            }
        }
        public static bool DetectWin()
        {
            if(GlobalVars.currentCol == GlobalVars.goalCol)
            {
                if(GlobalVars.currentRow == GlobalVars.goalRow)
                {
                    if(GlobalVars.currentFloor == GlobalVars.goalFloor)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
	}
}