﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LayersGamePrototype
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NavigatorTabs : TabbedPage
	{
		public NavigatorTabs ()
		{
			InitializeComponent ();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavigatorTabs)}:  ctor");
            SetIcons();
            this.CurrentPageChanged += (object sender, EventArgs e) => {
                var i = this.Children.IndexOf(this.CurrentPage);
                switch (i)
                {
                    case 0: GlobalVars.viewFloor = 1;
                        break;
                    case 1: GlobalVars.viewFloor = 2;
                        break;
                    case 2: GlobalVars.viewFloor = 3;
                        break;
                    case 3: GlobalVars.viewFloor = 4;
                        break;
                    default: GlobalVars.viewFloor = 1;
                        break;
                }
            };
            MessagingCenter.Subscribe<Application>(this, "t", (sender) =>
            {
                //Detects if the user is starting a game
                SetIcons();
            });
            MessagingCenter.Subscribe<Application>(this, "v", (sender) =>
            {
                DisplayAlert("YOU WIN", "Click below to continue", "OK");
                ((App)Application.Current).LastPuzz(GlobalVars.lastPuz);
                Navigation.PushAsync(new SplashPage());
            });
            Xamarin.Forms.PlatformConfiguration.AndroidSpecific.TabbedPage.SetIsSwipePagingEnabled(this, false);
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            //GlobalVars.viewFloor = 1;
        }
        public void SetIcons()
        {
            Children.Clear();
            var F1 = new NavigationPage(new Floor1());
            F1.Title = "Floor";
            F1.Icon = "number1.png";
            Children.Add(F1);
            if (GlobalVars.maxLevel > 1)
            {
                var F2 = new NavigationPage(new Floor2());
                F2.Title = "Floor";
                F2.Icon = "number2.png";
                Children.Add(F2);
                if (GlobalVars.maxLevel > 2)
                {
                    var F3 = new NavigationPage(new Floor3());
                    F3.Title = "Floor";
                    F3.Icon = "number3.png";
                    Children.Add(F3);
                    if (GlobalVars.maxLevel > 3)
                    {
                        var F4 = new NavigationPage(new Floor4());
                        F4.Title = "Floor";
                        F4.Icon = "number4.png";
                        Children.Add(F4);
                    }
                }
            }
        }
        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}