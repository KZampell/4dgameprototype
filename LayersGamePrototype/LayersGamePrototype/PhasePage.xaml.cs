﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LayersGamePrototype
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PhasePage : MasterDetailPage
    {
        public PhasePage ()
		{
            InitializeComponent ();
            MessagingCenter.Subscribe<Application>(this, "c", (sender) =>
            {
                this.IsPresented = false;
            });
            NavigationPage.SetHasNavigationBar(this, false);
            Navigation.PushAsync(new SplashPage());
        }
    }
}