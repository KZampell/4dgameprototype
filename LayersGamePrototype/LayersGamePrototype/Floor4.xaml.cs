﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LayersGamePrototype
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Floor4 : ContentPage
	{
        public string CurP
        {
            get
            {
                return string.Format("Active Phase: {0}", GlobalVars.currentPhase);
            }
        }
        public Floor4 ()
		{
			InitializeComponent ();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Floor4)}:  ctor");
            MessagingCenter.Subscribe<Application>(this, "c", (sender) =>
            {
                if (GlobalVars.viewFloor == 4)
                {
                    Pieces.PlacePieces(4, FloorFourGrid);
                }
                PlayerPhaseLabel.Text = CurP;
            });
            MessagingCenter.Subscribe<Application>(this, "n", (sender) =>
            {
                //Detects if the last visible floor before phase-change was this floor
                if (GlobalVars.viewFloor == 4)
                {
                    Pieces.SwapPieces(FloorFourGrid);
                }
            });
            BindingContext = this;
            NavigationPage.SetHasNavigationBar(this, false);
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            //GlobalVars.viewFloor = 4;
            Pieces.PlacePieces(4, FloorFourGrid);
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}