﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LayersGamePrototype
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Floor1 : ContentPage
	{
        public string CurP
        {
            get
            {
                return string.Format("Active Phase: {0}", GlobalVars.currentPhase);
            }
        }
        public Floor1 ()
		{
			InitializeComponent ();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Floor1)}:  ctor");
            MessagingCenter.Subscribe<Application>(this, "c", (sender) =>
            {
                //Detects if the last visible floor before phase-change was this floor
                if (GlobalVars.viewFloor == 1)
                {
                    Pieces.PlacePieces(1, FloorOneGrid);
                }
                PlayerPhaseLabel.Text = CurP;
            });
            MessagingCenter.Subscribe<Application>(this, "n", (sender) =>
            {
                //Detects if the last visible floor before phase-change was this floor
                if (GlobalVars.viewFloor == 1)
                {
                    Pieces.SwapPieces(FloorOneGrid);
                }
            });
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = this;
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
            //Update position of the player piece and board.
            //GlobalVars.viewFloor = 1;
            Pieces.PlacePieces(1, FloorOneGrid);
        }
        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}