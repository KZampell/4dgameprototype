﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Connectivity;
using System.Net.Http;
using ModernHttpClient;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LayersGamePrototype
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashPage : ContentPage
	{
        private ImageSource srci;
        public ImageSource ImgSrc
        {
            get
            {
                return srci;
            }
            set
            {
                srci = value;
                OnPropertyChanged(nameof(ImgSrc));
            }
        }
        private int plvl;
        public string LvL
        {
            get
            {
                return string.Format("Puzzle Difficulty {0}", plvl);
            }
            set
            {
                OnPropertyChanged(nameof(LvL));
            }
        }
        event ConnectivityChangedEventHandler ConnectivityChanged;
        public SplashPage ()
		{
            InitializeComponent ();
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    //Popup notification that user has no internet connection active.
                    await DisplayAlert("Disconnected", "You have lost your internet connection.\nPlease reconnect to continue using this application.", "OK");
                }
            };
            GlobalVars.lastPuz = ((App)Application.Current).LastPuzz();
            plvl = GlobalVars.lastPuz;
            BindingContext = this;
            //Hides the hamburger at the top (for accessing Phase menu)
            NavigationPage.SetHasNavigationBar(this, false);
            HandleImg();
        }
        public async void HandleImg()
        {
            //Image fetcher
            if (CrossConnectivity.Current.IsConnected)
            {
                var client = new HttpClient(new NativeMessageHandler());
                var uri = new Uri(
                    string.Format($"https://randomfox.ca/floof/"));
                //Sends the HTTP request
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");
                //Receives the response from the server
                HttpResponseMessage response = await client.SendAsync(request);
                Fox fux = null;
                if (response.IsSuccessStatusCode)
                {
                    //Converts the data into something usable
                    var content = await response.Content.ReadAsStringAsync();
                    fux = Fox.FromJson(content);
                    try { ImgSrc = ImageSource.FromUri(new Uri(fux.Image)); }
                    catch (SystemException e)
                    {

                    }
                }
            }
        }
        public void HandleButton(object sender, System.EventArgs e)
        {
            //When the user presses the difficulty button, fetch new image
            HandleImg();
            plvl++;
            if(plvl > 15) { plvl = 1; }
            LvL = "";
        }
        public void HandleGo(object sender, System.EventArgs e)
        {
            ((App)Application.Current).LoadBoard(plvl);
            try { MessagingCenter.Send<Application>(Application.Current, "t"); }
            catch(System.Reflection.TargetInvocationException pre)
            {
                //This is a rare issue, but it happens.
            }
            MessagingCenter.Send<Application>(Application.Current, "c");
            GlobalVars.lastPuz = plvl;
            Navigation.PopAsync();
        }
        public class ConnectivityChangedEventArgs : EventArgs
        {
            public bool IsConnected { get; set; }
        }

        public delegate void ConnectivityChangedEventHandler(object sender, ConnectivityChangedEventArgs e);
        

    }
}